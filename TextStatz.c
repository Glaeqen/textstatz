#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <wctype.h>
#include <wchar.h>
#include <time.h>

#define MINWORD 0

char *renameFilename(char *, char *);
int hashFunc(wchar_t);
wchar_t hashFuncInv(int);
void wordManager(wchar_t ***, int **, int *, wchar_t *);

char *reformatText(char *);
void performCalc(char *);

int main(int argc, char **argv){
	setlocale(LC_ALL, "");
	if(argc!=2){
		printf("Flawed args. Input directory.\n");
    return EXIT_FAILURE;
	}
	// reformatText:--> Arg: URL to file; formats text file; returns URL to file "name_formatted.txt"
	performCalc(reformatText(argv[1]));
	printf("Done. %s file reformatted and calculated.\n", argv[1]);
	return 0;
}

char *renameFilename(char *str1, char *str2){
	int strLength1 = 0;
	int strLength2 = 0;
	char *strLength1Pointer = str1;
	char *strLength2Pointer = str2;
	while(*strLength1Pointer){
		strLength1++;
		strLength1Pointer++;
	}
	while(*strLength2Pointer){
		strLength2++;
		strLength2Pointer++;
	}
	int strLength3 = strLength1+strLength2-3;
	char *str3;
	str3 = calloc(strLength3, sizeof(*str3));
	for(int i=0; i<strLength1-4; i++){
		str3[i] = str1[i];
	}
	for(int i=0; i<strLength2 ; i++){
		str3[i+strLength1-4] = str2[i];
	}
	return str3;
}
int hashFunc(wchar_t c){
	switch(c){
		case 'a':	return 1;
		case 'b':	return 2;
		case 'c':	return 3;
		case 'd':	return 4;
		case 'e':	return 5;
		case 'f':	return 6;
		case 'g':	return 7;
		case 'h':	return 8;
		case 'i':	return 9;
		case 'j':	return 10;
		case 'k':	return 11;
		case 'l':	return 12;
		case 'm':	return 13;
		case 'n':	return 14;
		case 'o':	return 15;
		case 'p':	return 16;
		case 'q':	return 17;
		case 'r':	return 18;
		case 's':	return 19;
		case 't':	return 20;
		case 'u':	return 21;
		case 'v':	return 22;
		case 'w':	return 23;
		case 'x':	return 24;
		case 'y':	return 25;
		case 'z':	return 26;
		
		case L'ą':	return 27;
		case L'ć':	return 28;
		case L'ę':	return 29;
		case L'ł':	return 30;
		case L'ń':	return 31;
		case L'ó':	return 32;
		case L'ś':	return 33;
		case L'ź':	return 34;
		case L'ż':	return 35;
		case L'é':	return 36;
		default:
			// Should never be used. If so, code is flawed or program is forced to work with unknown alphabet.
			// Approved alphabets: Latin; Polish;
			return 0;
	}
}
wchar_t hashFuncInv(int i){
	switch(i){
		case 1:	return 'a';
		case 2:	return 'b';
		case 3:	return 'c';
		case 4:	return 'd';
		case 5:	return 'e';
		case 6:	return 'f';
		case 7:	return 'g';
		case 8:	return 'h';
		case 9:	return 'i';
		case 10:	return 'j';
		case 11:	return 'k';
		case 12:	return 'l';
		case 13:	return 'm';
		case 14:	return 'n';
		case 15:	return 'o';
		case 16:	return 'p';
		case 17:	return 'q';
		case 18:	return 'r';
		case 19:	return 's';
		case 20:	return 't';
		case 21:	return 'u';
		case 22:	return 'v';
		case 23:	return 'w';
		case 24:	return 'x';
		case 25:	return 'y';
		case 26:	return 'z';
		
		case 27:	return L'ą';
		case 28:	return L'ć';
		case 29:	return L'ę';
		case 30:	return L'ł';
		case 31:	return L'ń';
		case 32:	return L'ó';
		case 33:	return L'ś';
		case 34:	return L'ź';
		case 35:	return L'ż';
		case 36:	return L'é';
		default:
			// Should never be used. If so, code is flawed or program is forced to work with unknown alphabet.
			// Approved alphabets: Latin; Polish;
			return '?';
	}
}
void wordManager(wchar_t ***wordArray, int **wordAmount, int *volumeWordAmount, wchar_t *newWord){
	static int isArrayInitialised = 0;
	static int isArrayEmpty = 1;
	static int arrayIndex = 0;
	int wordLocation;
	if(!isArrayInitialised){
		*wordArray = malloc(sizeof(**wordArray)*1);
		if(*wordArray==NULL){
			printf("Error mallocing wordArray");
			exit(EXIT_FAILURE);
		}
		*wordAmount = calloc(1, sizeof(*wordAmount));
		if(*wordAmount==NULL){
			printf("Error callocing wordAmount");
			exit(EXIT_FAILURE);
		}
		isArrayInitialised = 1;
	}
	if(isArrayEmpty){
		(*wordArray)[0] = malloc(sizeof(***wordArray)*(wcslen(newWord)+1));
		wcscpy(*wordArray[0], newWord);
		(*wordAmount)[0] = 1;
		isArrayEmpty = 0;
	}
	else{
		//Check if word is already in an array.
		wordLocation = 0;
		for(; wordLocation<arrayIndex+1; wordLocation++){
			if(!wcscmp((*wordArray)[wordLocation], newWord))	break;
		}
		//Word hasn't been found. Then:
		if(wordLocation==arrayIndex+1){
			arrayIndex++; //Increase arrays' volume.
			*wordArray = realloc(*wordArray, (arrayIndex+1)*sizeof(**wordArray));
			if(*wordArray==NULL){
				printf("Error reallocating wordArray memory.\n Array index: %d", arrayIndex);
				exit(EXIT_FAILURE);
			}
			*wordAmount = realloc(*wordAmount, (arrayIndex+1)*sizeof(**wordAmount));
			if(*wordAmount==NULL){
				printf("Error reallocating wordAmount memory.\n Array index: %d", arrayIndex);
				exit(EXIT_FAILURE);
			}
			(*wordArray)[arrayIndex] = malloc(sizeof(***wordArray)*(wcslen(newWord)+1));
			wcscpy((*wordArray)[arrayIndex], newWord);
			(*wordAmount)[arrayIndex] = 1;
		}
		//Word has been found in an array.
		else{
			((*wordAmount)[wordLocation])++;
		}
	}
	*volumeWordAmount = arrayIndex + 1;
}

// Removes uppercases, control characters and whitespaces.
char *reformatText(char *source){
	FILE *file = fopen(source, "r");
	char *formattedFilename = renameFilename(source, "_formatted.txt");
	FILE *fileEditedCopy = fopen(formattedFilename, "w");
	if(!file){
		printf("Flawed text file/directory.\n");
		exit(EXIT_FAILURE);
	}
	wchar_t s[255] = {0};
	while(!feof(file)){
		fscanf(file, "%ls ", s);
		wchar_t *tmpS = s;
		while(*tmpS){
			int timesNonAlphanumericEncountered = 1; //Set to '1' to let it check 'iswalpha' at least once.
			wchar_t *movingStrPointer;
			for(int i=1; i<=timesNonAlphanumericEncountered; i++){
				if(!iswalpha(*tmpS)){
					movingStrPointer = tmpS;
					while(*movingStrPointer){
						*movingStrPointer = *(movingStrPointer+1);
						movingStrPointer++;
						if(!iswalpha(*movingStrPointer))	timesNonAlphanumericEncountered++;
					}
				}
			}
			*tmpS = towlower(*tmpS);
			tmpS++;
		}
		//Checks if string is flawed; If yes, then reject
		int areUEvenAString = 1;
		wchar_t *areUEvenAStringPointer = s;
		do{
			if(iswcntrl(*areUEvenAStringPointer)) areUEvenAString = 0;
			areUEvenAStringPointer++;
		}while(*areUEvenAStringPointer);
		if(areUEvenAString)	fprintf(fileEditedCopy, "%ls ", s);
		//if(areUEvenAString)	printf("%ls ", s);
	}
	fclose(file);
	fclose(fileEditedCopy);
	return formattedFilename;
}
void performCalc(char *source){
	//Data for statistics:
	//Letters:
	int letterFrequency[37] = {0};
	int letterCounter = 0;
	int pairLetterFrequency[37][37] = {{0}};
	//Words:
	int wordCounter = 0;
	wchar_t **wordArray;
	int *wordAmount;
	int volumeWordAmount;
	//File managing:
	FILE *file = fopen(source, "r");
	FILE *statsOutput = fopen(renameFilename(source, "_stats.txt"), "w");
	FILE *statsWordsOutput = fopen(renameFilename(source, "_word_stats.txt"), "w");
	free(source);
	if(!file){
		printf("Flawed formatted text file/directory.\n");
		exit(EXIT_FAILURE);
	}
	//Formatted text file. Native scheme of words is ->> %ls %ls %ls %ls %ls...
	//Work with words:
	wchar_t word[255] = {0};
	while(!feof(file)){
		fscanf(file, "%ls ", word);
		wordManager(&wordArray, &wordAmount, &volumeWordAmount, word);
		wordCounter++;
	}
	//Return to the beginning of the file.
	rewind(file);
	//Work with letters:
	wchar_t c = 0;
	wchar_t previousC;
	while(!feof(file)){
		previousC = c;
		fscanf(file, "%lc", &c);
		if(iswalpha(c)){
			if(!hashFunc(c))	printf("Unknown char: %lc\n", c);
			letterFrequency[hashFunc(c)]++;
			letterCounter++;
			if(previousC && iswalpha(previousC)){
				pairLetterFrequency[hashFunc(previousC)][hashFunc(c)]++;
			}
		}
	}
	fclose(file);
	//Results:
	fprintf(statsOutput, "%d\n", wordCounter);
	fprintf(statsOutput, "%d\n", letterCounter);
	for(int i=0; i<37; i++){
		fprintf(statsOutput, "%d\n", letterFrequency[i]);
	}
	printf("\n");
	for(int i=0; i<37; i++){
		for(int j=0; j<37; j++){
			fprintf(statsOutput, "%d\n", pairLetterFrequency[i][j]);
		}
	}
	for(int i=0; i<volumeWordAmount; i++){
		if(wordAmount[i]>MINWORD){
			fprintf(statsWordsOutput, "%ls : %d\n", wordArray[i], wordAmount[i]);	
		}
		free(wordArray[i]);
	}
	free(wordArray);
	free(wordAmount);
	fclose(statsOutput);
	fclose(statsWordsOutput);
}