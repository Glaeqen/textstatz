Simple C Program which purpose is to gain data about txt files.
It is:
Amount of letters (overall), specific letters, pair of letters (-ab-;-cd-),
amount of words (overall) and specific words.

Source includes .xlsx file which shows usage.
Input:
<name>.txt - Text file to analyse. Advised coding: UTF-8 (UNIX); ANSI (Win).
Output:
<name>_formatted.txt >> Reformated text file which contains raw lower-cased words to simplify calculations.
<name>_formatted_stats.txt >> Contains statistics of letters and amount of words in a handy raw-numbers format (for excel e.g.).
<name>_formatted_word_stats.txt >> Contains string representing word and its frequency of appearance in format >> <word> : <FOA>